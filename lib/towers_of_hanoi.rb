# Towers of Hanoi
#
# Write a Towers of Hanoi game:
# http://en.wikipedia.org/wiki/Towers_of_hanoi
#
# In a class `TowersOfHanoi`, keep a `towers` instance variable that is an array
# of three arrays. Each subarray should represent a tower. Each tower should
# store integers representing the size of its discs. Expose this instance
# variable with an `attr_reader`.
#
# You'll want a `#play` method. In a loop, prompt the user using puts. Ask what
# pile to select a disc from. The pile should be the index of a tower in your
# `@towers` array. Use gets
# (http://andreacfm.com/2011/06/11/learning-ruby-gets-and-chomp.html) to get an
# answer. Similarly, find out which pile the user wants to move the disc to.
# Next, you'll want to do different things depending on whether or not the move
# is valid. Finally, if they have succeeded in moving all of the discs to
# another pile, they win! The loop should end.
#
# You'll want a `TowersOfHanoi#render` method. Don't spend too much time on
# this, just get it playable.
#
# Think about what other helper methods you might want. Here's a list of all the
# instance methods I had in my TowersOfHanoi class:
# * initialize
# * play
# * render
# * won?
# * valid_move?(from_tower, to_tower)
# * move(from_tower, to_tower)
#
# Make sure that the game works in the console. There are also some specs to
# keep you on the right track:
#
# ```bash
# bundle exec rspec spec/towers_of_hanoi_spec.rb
# ```
#
# Make sure to run bundle install first! The specs assume you've implemented the
# methods named above.

class TowersOfHanoi
  def initialize
    @towers = [[3, 2, 1], [], []]
  end


  def play
    puts "Which pile would you like to select a disc from? 0, 1, or 2?"
    move_from = gets.to_i
    puts "On which pile would you like to place the disc?"
    move_to = gets.to_i
    move(move_from, move_to)
    puts @towers.to_s
    end

  def valid_move?(from_tower, to_tower)

    if from_tower > @towers.length || to_tower > @towers.length
      return false
    end

    if  @towers[from_tower][-1] != nil
      if @towers[to_tower][-1] == nil
        return true
      elsif @towers[from_tower][-1] < @towers[to_tower][-1]
         return true
      else
        return false
      end
    else
      return false
    end
  end

  def won?
    if @towers == [[], [], [3, 2, 1]] || @towers == [[], [3, 2, 1], []]
      return true
    else false
    end
  end


  def move(from_tower, to_tower)
    if valid_move?(from_tower, to_tower)
      disc = @towers[from_tower].pop
      puts "moving disc #{disc}"
      @towers[to_tower].push(disc)

    else
      puts "Please make a valid selection"
    end
  end

  def render
    turn = 0
    puts "Each array represents a tower, each number a ring with size corresponding to the numbers value: " + @towers.to_s.chomp
    until won?
    turn += 1
      play
    end
    puts "Congratulations, You Win!!! It took you #{turn} turns to finish. Nice Job!"
  end



  attr_accessor :towers
end
